if(sessionStorage.id_exclude!== true) {
   seltag.getItems = sessionStorage.id_exclude;
}

if(sessionStorage.seltag!==undefined) {
   seltag.selectedIndex = sessionStorage.seltag;
}       


$(function(){
    $('input:checkbox').each(function() {
    // Iterate over the checkboxes and set their "check" values based on the session data
        var $el = $(this);
        $el.prop('checked', sessionStorage[$el.prop('id')] === 'true');
    });

    $('input:checkbox').on('change', function() {
        // save the individual checkbox in the session inside the `change` event, 
        // using the checkbox "id" attribute
        var $el = $(this);
        sessionStorage[$el.prop('id')] = $el.is(':checked');
    });
});

// ajax
$(document).ready(function() {
    $('#gen-form').submit(function() {
        $.ajax({
            type: 'POST',
            url: '.',
            data: $('#gen-form').serialize(),
            success: function(html) {
                $('body').html(html)
            }
        })
        return false;
    })
})
