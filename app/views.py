from django.shortcuts import render
import random

default_range = 8

def index(request):
    if request.method == 'POST':
        param = request.POST['param']
        int_param = int(param)
        if request.POST.get('exclude'):
            latters = 'qwertyuiopasdfghjklzxcvbnm'
        else:
            latters = 'qwertyuiopasdfghjklzxcvbnm,~*-+=[]{};!@#()<>:?'
    else:
        int_param = 8
        latters = 'qwertyuiopasdfghjklzxcvbnm,~*-+=[]{};!@#()<>:?'

    print(int_param, '<---param') 

    numbers = list(range(0, 11)) * 9 # create list of 1 to 10
    str_numbers = [str(x) for x in numbers]



    latters = list(latters) # conver characters in a list
    copy_list = latters[:] * 4 # copy list
    upper_latters = [x.upper() for x in copy_list] # List of upper characters
    list_characters_for_password = str_numbers + latters + upper_latters

    list_password = []
    for x in range(int_param):
        tmp_password = random.choice(list_characters_for_password)
        # print(complete_password)
        list_password.append(tmp_password)
    
    complete_password = ''.join(list_password)
    print(list_password)
    print(complete_password)

    return render(request, 'app/index.html', locals())    